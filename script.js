/* 
**TodoList**:
Crea una aplicación que nos permita añadir tareas, editar y eliminarlas. Todo el mundo necesita una ayuda para recordar todas esas tareas que necesitamos hacer día a día.

feat: Include task
    check value:
    // click "add task" (EVENT CLICK) --> check input value
    //if empty value in input --> no insert task
    //if true value --> include task (return name task)

    include task:
    //make constructor with --> input-text (name task), checkbox (remove), button (edit)
    //appendchild to .list-group

feat: Edit task
    // click "edit" button (EVENT CLICK)
    // by default input-text have --> attribute readonly
    // remove attribute readonly
    // change text button (edit) --> "guardar"
    feat: Save task
        // Click button (edit) --> text button to "edit", and add attribute readonly

feat: Remove task
    // EVENT CHANGE : check is checked  https://stackoverflow.com/questions/6358673/javascript-checkbox-onchange
    // remove parent input-group
*/

/* const makeTask = () => {
    const inputText = document.createElement('input');
    inputText.setAttribute('readonly')
} */

const buttonAddTask = document.getElementById('addTask');
const listTask = document.getElementById('listTask');

const includeTask = (e) => {

    const makeInput = (text) => {
        const inputText = document.createElement('input')
        inputText.classList.add("form-control")
        inputText.setAttribute("type", "text");
        inputText.value = text;
        inputText.readOnly = true;
    
        const buttonEdit = document.createElement('button')
        buttonEdit.classList.add("btn", "btn-outline-secondary")
        buttonEdit.setAttribute("type", "button");
        buttonEdit.textContent = 'Edit';
    
        const inputRemove = document.createElement('input');
        inputRemove.classList.add("form-check-input", "mt-0");
        inputRemove.setAttribute("type", "checkbox");
    
        const warpInputRemove = document.createElement('div');
        warpInputRemove.classList.add("input-group-text");
    
        warpInputRemove.appendChild(inputRemove);
    
        const inputGroup = document.createElement('div');
        inputGroup.classList.add("input-group");
    
        inputGroup.appendChild(warpInputRemove);
        inputGroup.appendChild(inputText);
        inputGroup.appendChild(buttonEdit);
    
        return inputGroup;
    }

    const inputText = e.target.nextElementSibling;
    const inputTextValue = inputText.value;

    if( inputTextValue.length !== 0) {
        listTask.appendChild(makeInput(inputTextValue));
        inputText.value = '';
        inputText.focus();
    }
    inputText.focus();
}

buttonAddTask.addEventListener('click', includeTask )

const editTask = (e) => {
    if (e.target.tagName === 'BUTTON') {
        const groupInput = e.target.closest(".input-group");
        const inputText = groupInput.querySelector('[type="text"]');
        const buttonEdit = groupInput.querySelector('button');
        if (buttonEdit.textContent === 'Edit') {
            inputText.readOnly = false;
            buttonEdit.textContent = 'Save';
        } else {
            inputText.readOnly = true;
            buttonEdit.textContent = 'Edit';
        }
    }
}

const removeTask = (e) => {
    if (e.target.getAttribute('type') === 'checkbox') {
        const Removecheckbox = e.target;
        const groupInput = e.target.closest(".input-group");
        if (Removecheckbox.checked) {
            setTimeout( () => {
                groupInput.remove();
            }, 200);
        }
    }
}

listTask.addEventListener('click', (e) => {
    editTask(e);
    removeTask(e);
} )
